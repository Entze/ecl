module EulerCommonLib.Logic where

(~>) True False = False
(~>) _ _ = True

(===) True True = True
(===) False False = True
(===) _ _ = False

(=/=) True True = False
(=/=) False False = False
(=/=) _ _ = True


data Operator = UnaryOperator UnaryOperator | BinaryOperator BinaryOperator deriving Eq

instance Show Operator where
    show (UnaryOperator op) = show op
    show (BinaryOperator op) = show op

data UnaryOperator = Not deriving (Eq, Enum)

instance Show UnaryOperator where
    show Not = "¬"

unaryLogic Not = not

unaryOperatorToOperator :: UnaryOperator -> Operator
unaryOperatorToOperator x = UnaryOperator x

data BinaryOperator = And | Or | Implies | Equivalent | Xor deriving (Eq, Enum)

instance Show BinaryOperator where
    show And = "∧"
    show Or = "∨"
    show Implies = "→"
    show Equivalent = "↔"
    show Xor = "⊻"

binaryLogic And = (&&)
binaryLogic Or = (||)
binaryLogic Implies = (~>)
binaryLogic Equivalent = (===)
binaryLogic Xor = (=/=)

binaryOperatorToOperator :: BinaryOperator -> Operator
binaryOperatorToOperator x = BinaryOperator x

operatorPrecedence :: Operator -> Int
operatorPrecedence (UnaryOperator _) = 10
operatorPrecedence (BinaryOperator And) = 4
operatorPrecedence (BinaryOperator Or) = 4
operatorPrecedence (BinaryOperator Implies) = 5
operatorPrecedence (BinaryOperator Equivalent) = 6
operatorPrecedence (BinaryOperator Xor) = 6

simpleMapper :: Eq a => [(a,b)] -> a -> b
simpleMapper ((a,b):ab) e
    | a == e = b
    | otherwise = simpleMapper ab e

simpleMapperDefault :: Eq a => b -> [(a,b)] -> a -> b
simpleMapperDefault d ((a,b):ab) e
    | a == e = b
    | otherwise = simpleMapperDefault d ab e
simpleMapperDefault d [] _ = d

positiveAtoms :: Eq a => [a] -> (a -> Bool)
positiveAtoms ls = (flip elem) ls

negativeAtoms :: Eq a => [a] -> (a -> Bool)
negativeAtoms ls = (flip notElem) ls

allAssignments :: Int -> [[Bool]]
allAssignments i = nextAssignments (replicate i False)
    where
      nextAssignments a
          | allTrue = [a]
          | otherwise = a:nextAssignments (next a)
          where
            allTrue = and a
      next (False:ls) = (True:ls)
      next (True:ls) = (False:(next ls))
      next [] = []
