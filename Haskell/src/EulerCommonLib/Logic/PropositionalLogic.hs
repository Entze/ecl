module EulerCommonLib.Logic.PropositionalLogic where

import Prelude hiding (map)
import qualified Prelude
import qualified Data.Set
--import qualified Data.Map
import qualified EulerCommonLib.Logic as ECL.L


data PropositionalFormula a = Final Bool | Atom a | Unary ECL.L.UnaryOperator (PropositionalFormula a) | Binary ECL.L.BinaryOperator (PropositionalFormula a) (PropositionalFormula a) deriving Eq

propositionalFormulaPrecedence (Atom _) = 0
propositionalFormulaPrecedence (Final _) = 0
propositionalFormulaPrecedence x = (ECL.L.operatorPrecedence . getOperator) x


instance (Show a) => Show (PropositionalFormula a) where
    showsPrec _ (Final True) = showString "⊤"
    showsPrec _ (Final False) = showString "⊥"
    showsPrec _ (Atom a) = (showString . show) a
    showsPrec d f@(Unary op s) = showParen (p < d) (shows op . showsPrec p s)
        where
          p = propositionalFormulaPrecedence f
    showsPrec d f@(Binary op s1 s2) = showParen (p <= d) (showsPrec p s1 . showString " " . shows op . showString " " . showsPrec p s2)
        where
          p = propositionalFormulaPrecedence f


instance Foldable PropositionalFormula where
    foldMap f (Final _) = mempty
    foldMap f (Atom a) = f a
    foldMap f (Unary _ a) = foldMap f a
    foldMap f (Binary _ a b) = foldMap f a `mappend` foldMap f b

immediateSubFormulas (Unary _ f) = [f]
immediateSubFormulas (Binary _ f1 f2) = [f1, f2]
immediateSubFormulas x = []

subFormulaList a@(Unary _ f) = a:subFormulaList f
subFormulaList a@(Binary _ f1 f2) = a:subFormulaList f1 ++ subFormulaList f2
subFormulaList a = [a]

reformulate (ECL.L.UnaryOperator op) [a] = Unary op a
reformulate (ECL.L.BinaryOperator op) [a,b] = Binary op a b

getOperator :: PropositionalFormula a -> ECL.L.Operator
getOperator (Unary op _) = ECL.L.unaryOperatorToOperator op
getOperator (Binary op _ _) = ECL.L.binaryOperatorToOperator op

isAtom (Final _) = True
isAtom (Atom _) = True
isAtom x = False

simplifyRuleOrVerum :: PropositionalFormula a -> PropositionalFormula a
simplifyRuleOrVerum (Binary ECL.L.Or (Final True) _) = Final True
simplifyRuleOrVerum (Binary ECL.L.Or _ (Final True)) = Final True
simplifyRuleOrVerum x = x

simplifyRuleAndVerum :: PropositionalFormula a -> PropositionalFormula a
simplifyRuleAndVerum (Binary ECL.L.And (Final True) f) = f
simplifyRuleAndVerum (Binary ECL.L.And f (Final True)) = f
simplifyRuleAndVerum x = x

simplifyRuleOrFalsum :: PropositionalFormula a -> PropositionalFormula a
simplifyRuleOrFalsum (Binary ECL.L.Or (Final False) f) = f
simplifyRuleOrFalsum (Binary ECL.L.Or f (Final False)) = f
simplifyRuleOrFalsum x = x

simplifyRuleAndFalsum :: PropositionalFormula a -> PropositionalFormula a
simplifyRuleAndFalsum (Binary ECL.L.And (Final False) _) = Final False
simplifyRuleAndFalsum (Binary ECL.L.And _ (Final False)) = Final False
simplifyRuleAndFalsum x = x

simplifyRuleNotFinal :: PropositionalFormula a -> PropositionalFormula a
simplifyRuleNotFinal (Unary ECL.L.Not (Final b)) = Final (not b)
simplifyRuleNotFinal x = x

simplifyRuleDoubleNot :: PropositionalFormula a -> PropositionalFormula a
simplifyRuleDoubleNot (Unary ECL.L.Not (Unary ECL.L.Not f)) = f
simplifyRuleDoubleNot x = x

simplifyRuleTertiumNonDatur :: (Eq a) => PropositionalFormula a -> PropositionalFormula a
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Or f1 (Unary ECL.L.Not f2))
                            | f1 == f2 = Final True
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Or (Unary ECL.L.Not f1) f2)
                            | f1 == f2 = Final True
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.And (Unary ECL.L.Not f1) f2)
                            | f1 == f2 = Final False
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.And f1 (Unary ECL.L.Not f2))
                            | f1 == f2 = Final False
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Implies f1 f@(Unary ECL.L.Not f2))
                            | f1 == f2 = f
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Implies (Unary ECL.L.Not f1) f2)
                            | f1 == f2 = f1
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Equivalent f1 (Unary ECL.L.Not f2))
                            | f1 == f2 = Final False
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Equivalent (Unary ECL.L.Not f1) f2)
                            | f1 == f2 = Final False
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Xor f1 (Unary ECL.L.Not f2))
                            | f1 == f2 = Final True
                            | otherwise = x
simplifyRuleTertiumNonDatur x@(Binary ECL.L.Xor (Unary ECL.L.Not f1) f2)
                            | f1 == f2 = Final True
                            | otherwise = x
simplifyRuleTertiumNonDatur x = x

simplifyRuleSelf :: (Eq a) => PropositionalFormula a -> PropositionalFormula a
simplifyRuleSelf x@(Binary ECL.L.Or f1 f2)
    | f1 == f2 = f1
    | otherwise = x
simplifyRuleSelf x@(Binary ECL.L.And f1 f2)
    | f1 == f2 = f1
    | otherwise = x
simplifyRuleSelf x@(Binary ECL.L.Implies f1 f2)
    | f1 == f2 = Final True
    | otherwise = x
simplifyRuleSelf x@(Binary ECL.L.Equivalent f1 f2)
    | f1 == f2 = Final True
    | otherwise = x
simplifyRuleSelf x@(Binary ECL.L.Xor f1 f2)
    | f1 == f2 = Final False
    | otherwise = x
simplifyRuleSelf x = x

simplifyRules :: [PropositionalFormula a -> PropositionalFormula a]
simplifyRules = [simplifyRuleOrVerum, simplifyRuleAndVerum, simplifyRuleOrFalsum, simplifyRuleAndFalsum, simplifyRuleNotFinal, simplifyRuleDoubleNot]

simplify :: PropositionalFormula a -> PropositionalFormula a
simplify = applyBottomToTop (foldl1 (.) simplifyRules)

reductionRules :: (Eq a) => [PropositionalFormula a -> PropositionalFormula a]
reductionRules = simplifyRules ++ [simplifyRuleSelf, simplifyRuleTertiumNonDatur]

reduce :: (Eq a) => PropositionalFormula a -> PropositionalFormula a
reduce = apply (foldl1 (.) reductionRules)

expandDeMorgan :: PropositionalFormula a -> PropositionalFormula a
expandDeMorgan (Unary ECL.L.Not (Binary ECL.L.And f1 f2)) = Binary ECL.L.Or (Unary ECL.L.Not f1) (Unary ECL.L.Not f2)
expandDeMorgan (Unary ECL.L.Not (Binary ECL.L.Or f1 f2)) = Binary ECL.L.And (Unary ECL.L.Not f1) (Unary ECL.L.Not f2)
expandDeMorgan x = x

expandRules :: [PropositionalFormula a -> PropositionalFormula a]
expandRules = [expandDeMorgan]

expand :: PropositionalFormula a -> PropositionalFormula a
expand = applyBottomToTop (foldl1 (.) expandRules)

equivalentTransformationSwapBinary :: PropositionalFormula a -> PropositionalFormula a
equivalentTransformationSwapBinary (Binary op f1 f2) = Binary op f2 f1
equivalentTransformationSwapBinary x = x

equivalentTransformationEquivalentToImplies :: PropositionalFormula a -> PropositionalFormula a
equivalentTransformationEquivalentToImplies (Binary ECL.L.Equivalent f1 f2) = Binary ECL.L.And (Binary ECL.L.Implies f1 f2) (Binary ECL.L.Implies f2 f1)
equivalentTransformationEquivalentToImplies x = x

equivalentTransformationXorToImplies :: PropositionalFormula a -> PropositionalFormula a
equivalentTransformationXorToImplies (Binary ECL.L.Xor f1 f2) = Binary ECL.L.Or (Unary ECL.L.Not (Binary ECL.L.Implies f1 f2)) (Unary ECL.L.Not (Binary ECL.L.Implies f2 f1))
equivalentTransformationXorToImplies x = x

equivalentTransformationImpliesToOr :: PropositionalFormula a -> PropositionalFormula a
equivalentTransformationImpliesToOr (Binary ECL.L.Implies f1 f2) = Binary ECL.L.Or (Unary ECL.L.Not f1) f2
equivalentTransformationImpliesToOr x = x

equivalentTransformations :: [PropositionalFormula a -> PropositionalFormula a]
equivalentTransformations = [equivalentTransformationSwapBinary, equivalentTransformationEquivalentToImplies, equivalentTransformationXorToImplies, equivalentTransformationImpliesToOr]

nnfRules :: [PropositionalFormula a -> PropositionalFormula a]
nnfRules = equivalentTransformationEquivalentToImplies:equivalentTransformationXorToImplies:equivalentTransformationImpliesToOr:expandDeMorgan:simplifyRules

toNNF :: PropositionalFormula a -> PropositionalFormula a
toNNF = apply' (foldl1 (.) nnfRules)

applyBottomToTop f a@(Final _) = f a
applyBottomToTop f a@(Atom _) = f a
applyBottomToTop f (Unary op b) = f (Unary op (f b))
applyBottomToTop f (Binary op b1 b2) = f (Binary op (f b1) (f b2))

applyTopToBottom :: (PropositionalFormula a -> PropositionalFormula a) -> PropositionalFormula a -> PropositionalFormula a
applyTopToBottom f a@(Final _) = f a
applyTopToBottom f a@(Atom _) = f a
applyTopToBottom f a = if atom then b else reformulate (getOperator b) s
    where
      s = Prelude.map (f `applyTopToBottom` ) (immediateSubFormulas b)
      atom = isAtom b
      b = f a

reapply :: Eq a => (PropositionalFormula a -> PropositionalFormula a) -> PropositionalFormula a -> PropositionalFormula a
reapply _ a@(Final _) = a
reapply _ a@(Atom _) = a
reapply f a
    | a == b = a
    | atom = b
    | otherwise = reformulate (getOperator b) s
    where
      s = Prelude.map (f `reapply`) (immediateSubFormulas b)
      atom = isAtom b
      b = f a

apply :: Eq a => (PropositionalFormula a -> PropositionalFormula a) -> PropositionalFormula a -> PropositionalFormula a
apply f a@(Final _) = f a
apply f a@(Atom _) = f a
apply f (Unary op b) = (reapply f . f) (Unary op ((reapply f . f) b))
apply f (Binary op b1 b2) = (reapply f . f) (Binary op ((reapply f . f) b1) ((reapply f . f) b2))

apply' :: (PropositionalFormula a -> PropositionalFormula a) -> PropositionalFormula a -> PropositionalFormula a
apply' f = applyBottomToTop (f `applyTopToBottom`)

map :: (a -> b) -> PropositionalFormula a -> PropositionalFormula b
map f (Atom a) = Atom (f a)
map _ (Final b) = Final b
map f (Unary op a) = Unary op (map f a)
map f (Binary op a b) = Binary op (map f a) (map f b)


mapToBool :: (a -> Bool) -> PropositionalFormula a -> PropositionalFormula Bool
mapToBool f (Atom a) = Final (f a)
mapToBool f p = map f p

evaluate :: PropositionalFormula Bool -> Bool
evaluate (Atom b) = b
evaluate (Final b) = b
evaluate (Unary op f) = ECL.L.unaryLogic op (evaluate f)
evaluate (Binary op f1 f2) = ECL.L.binaryLogic op (evaluate f1) (evaluate f2)

naturalDeductionAndElemination1 :: Eq a => PropositionalFormula a -> [PropositionalFormula a]
naturalDeductionAndElemination1 f = []

atomsAsList :: PropositionalFormula a -> [a]
atomsAsList (Atom a) = [a]
atomsAsList (Final _) = []
atomsAsList (Unary _ f) = atomsAsList f
atomsAsList (Binary _ f1 f2) = atomsAsList f1 ++ atomsAsList f2

uniqueAtomsAsList :: Eq a => PropositionalFormula a -> [a]
uniqueAtomsAsList (Atom a) = [a]
uniqueAtomsAsList (Final _) = []
uniqueAtomsAsList (Unary _ f) = uniqueAtomsAsList f
uniqueAtomsAsList (Binary _ f1 f2) = f
    where
      f = f1' ++ (filter (`notElem` f1') f2')
      f1' = uniqueAtomsAsList f1
      f2' = uniqueAtomsAsList f2

uniqueAtomsAsSet :: Ord a => PropositionalFormula a -> Data.Set.Set a
uniqueAtomsAsSet (Atom a) = Data.Set.singleton a
uniqueAtomsAsSet (Final _) = Data.Set.empty
uniqueAtomsAsSet (Unary _ f) = uniqueAtomsAsSet f
uniqueAtomsAsSet (Binary _ f1 f2) = Data.Set.union (uniqueAtomsAsSet f1) (uniqueAtomsAsSet f2)

allVariableAssignmentsFromList :: Eq a => [a] -> [(a -> Bool)]
allVariableAssignmentsFromList ls = Prelude.map ECL.L.simpleMapper (zipWith zip (repeat ls) (ECL.L.allAssignments len))
    where
      len = length ls

            {--

--Prelude.map (evaluate . ((flip mapToBool) x)) (allVariableAssignmentsFromList "ab")

truthTable :: Eq a => PropositionalFormula a -> [([(a, Bool)], Bool)]
truthTable f =
    where
      atoms = unqueAtomsAsList f
      ava = allVariableAssignmentsFromList atoms
      --}

--allVariableAssignmentsFromSet :: Ord a => Data.Set.Set a -> [(a -> Bool)]
--allVariableAssignmentsFromSet s = Prelude.map Data.Set.member (Data.Set.toList (Data.Set.powerSet s))
