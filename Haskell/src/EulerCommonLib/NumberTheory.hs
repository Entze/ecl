module EulerCommonLib.NumberTheory (
                     fibonnaciSequence,
                     fibonacciNumber,
                     selfAddingSequence,
                     dualAddingSequence
                    ) where


fibonacciNumber :: (Integral a, Integral b) => a -> b
fibonacciNumber a = fibonnaciSequence !! (fromIntegral a)


fibonnaciSequence :: Integral int => [int]
fibonnaciSequence = dualAddingSequence 0 1


dualAddingSequence :: Integral int => int -> int -> [int]
dualAddingSequence s1 s2 = s1:s2:(dualAddingSequence' s1 s2)
    where
      dualAddingSequence' n1 n2 = n:(dualAddingSequence' n2 n)
          where
            n = n1 + n2



selfAddingSequence :: Integral int => [int] -> [int]
selfAddingSequence ls = ls ++ (selfAddingSequence' ls)
    where
      selfAddingSequence' ll@(l:ls) = next : (selfAddingSequence' nextLs)
          where
            nextLs = ls ++ [next]
            next = sum ll
